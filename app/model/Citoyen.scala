package model

import play.api.libs.json._
import reactivemongo.bson.BSONObjectID
import reactivemongo.play.json._

case class Citoyen(_id : Option[BSONObjectID],
                   name: String,
                   gender: String,
                   age: Int,
                   state: String)

object Citoyen{
  implicit val format: OFormat[Citoyen] = Json.format[Citoyen]
}