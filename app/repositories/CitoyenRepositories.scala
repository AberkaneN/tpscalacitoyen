package repositories

import javax.inject.Inject

import scala.concurrent.{ExecutionContext, Future}
import play.modules.reactivemongo.ReactiveMongoApi
import reactivemongo.play.json._
import reactivemongo.play.json.collection.JSONCollection
import reactivemongo.bson.{BSONDocument, BSONObjectID}
import reactivemongo.api.{Cursor, ReadPreference}
import model.Citoyen
import reactivemongo.api.commands.WriteResult

class CitoyenRepositories @Inject()(
  implicit ec: ExecutionContext,
  reactiveMongoApi: ReactiveMongoApi
) {
  private def collection: Future[JSONCollection] = reactiveMongoApi.database.map(_.collection("citoyens"))

  def list(limit: Int = 100): Future[Seq[Citoyen]] = collection.flatMap(
    _.find(BSONDocument())
    .cursor[Citoyen](ReadPreference.primary)
    .collect[Seq](limit, Cursor.FailOnError[Seq[Citoyen]]())
  )

  def create(citoyen: Citoyen): Future[WriteResult] = collection.flatMap(_.insert(citoyen))

  def read(id: BSONObjectID): Future[Option[Citoyen]] = collection.flatMap(_.find(BSONDocument("_id" -> id)).one[Citoyen])

  def update(id: BSONObjectID, citoyen: Citoyen): Future[Option[Citoyen]] = collection.flatMap(_.findAndUpdate(BSONDocument("_id" -> id),BSONDocument(
    f"$$set" -> BSONDocument(
      "name" -> citoyen.name,
      "gender" -> citoyen.gender,
      "age" -> citoyen.age,
      "state" -> citoyen.state
    )
  ),
    true
    ).map(_.result[Citoyen])
  )

  def destroy(id: BSONObjectID): Future[Option[Citoyen]] = collection.flatMap(_.findAndRemove(BSONDocument("_id" -> id)).map(_.result[Citoyen]))
}
