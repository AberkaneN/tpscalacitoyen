package controllers


import javax.inject._


import scala.concurrent.{ExecutionContext,Future}
import play.api._
import play.api.mvc._
import play.api.libs.json._
import repositories.CitoyenRepositories
import reactivemongo.bson.BSONObjectID
import model.Citoyen

@Singleton
class CitoyenController @Inject()(
   implicit ec: ExecutionContext,
   components: ControllerComponents,
   citoyensRepo : CitoyenRepositories)
  extends AbstractController(components){

  def index() = Action { implicit request: Request[AnyContent] => Ok("App Works")}

  def listCitoyens = Action.async {
    citoyensRepo.list().map{ citoyens =>
      Ok(Json.toJson(citoyens))
    }
  }

  def createCitoyen = Action.async(parse.json) {
    _.body.validate[Citoyen]
      .map {
      citoyen => citoyensRepo.create(citoyen).map {
        _=> Created
      }
    }.getOrElse(Future.successful(BadRequest ("Invalid format")))
  }

  def readCitoyen(id: BSONObjectID) = Action.async {
    citoyensRepo.read(id).map { maybeCitoyen =>
      maybeCitoyen.map { citoyen =>
        Ok(Json.toJson(citoyen))
      }.getOrElse(NotFound)
    }
  }

def updateCitoyen(id: BSONObjectID) = Action.async(parse.json) {
  _.body.validate[Citoyen]
    .map {
      citoyen => citoyensRepo.update(id,citoyen).map {
        case Some(citoyen) => Ok(Json.toJson(citoyen))
        case _             => NotFound
      }
    }.getOrElse(Future.successful(BadRequest ("Invalid format")))
}

  def deleteCitoyen(id: BSONObjectID ) = Action.async {
    citoyensRepo.destroy(id).map{
      case Some(citoyen) => Ok(Json.toJson(citoyen))
      case _             => NotFound
    }
  }
}