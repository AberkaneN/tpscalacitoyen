package kafka

import java.util.Properties

import model.Citoyen
import org.apache.kafka.clients.producer.{Callback, KafkaProducer, ProducerConfig, ProducerRecord, RecordMetadata}
import reactivemongo.bson.BSONObjectID

object KafkaProducerCitoyen {

  val bootstrapServer = "127.0.0.1:9092"
  val groupId = "citoyen-id"
  val topic = "citoyen"

  val props: Properties = {
    val prop = new Properties()
    prop.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer)
    prop.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    prop.put("value.serializer", classOf[CustomSerializer])
    prop.put(ProducerConfig.ACKS_CONFIG, "all")
    prop
  }

  def main(args: Array[String]): Unit = {

    val callback = new Callback {
      override def onCompletion(metadata: RecordMetadata, exception: Exception): Unit = {
        println("Callback" + metadata.toString)
      }
    }

    val citoyen = Citoyen(_id = Option(BSONObjectID.generate()),name="naruto",gender="M",age=17,state="hokage")
    println(citoyen)
    val producer = new KafkaProducer[String, Citoyen](props)

    for (k <- 1 to 2) {
      producer.send(new ProducerRecord(topic, s"key ${k}", citoyen), callback)
    }
    producer.close()
  }
}
